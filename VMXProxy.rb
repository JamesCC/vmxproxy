require "socket"
require "serialport"

require "os"
require "optparse"
require_relative "VMixerConsumer"

VERSION="1.3.2"

options = {}
optparse = OptionParser.new do|opts|
    # Set a banner, displayed at the top
    # of the help screen.
    opts.banner = "\n"
    opts.banner+= "VMXProxy v"+VERSION+"\n"
    opts.banner+= "\n"
    opts.banner+= "Usage: VMXProxy [options] --serial DEV              # sim mode (serial)\n"
    opts.banner+= "       VMXProxy [options] --net PORT                # sim mode (network)\n"
    opts.banner+= "       VMXProxy [options] --net PORT --serial DEV   # server mode (network->serial)\n"
    opts.banner+= "\n"
  
    # Define the options, and what they do
    options[:verbose] = false
    opts.on( '-v', '--verbose', 'Output more information' ) do
        options[:verbose] = true
    end
  
    options[:serial] = nil
    opts.on( '-s', '--serial DEV', 'Supply serial port' ) do|s|
        options[:serial] = s
    end
  
    options[:baud_rate] = 115200
    options[:data_bits] = 8
    options[:stop_bits] = 1
    options[:parity] = SerialPort::NONE
    opts.on( '-b', '--baud BAUDRATE', 'Serial port baudrate' ) do|s|
        options[:baud] = s
    end

    options[:net] = nil
    opts.on( '-n', '--network PORT', 'Supply network port' ) do|s|
        options[:net] = s
    end

    options[:password] = nil
    opts.on( '-p', '--password PASSWORD', 'setup password (no spaces permitted)' ) do|s|
        options[:password] = s
    end
  
    options[:delay] = nil
    opts.on( '-d', '--delay MS_MAX', 'debug option to introduce a variable response delay' ) do|s|
        options[:delay] = s
    end
  
    # This displays the help screen, all programs are
    options[:discard] = nil
    opts.on( '-D', '--discard RATE', 'debug option to discard occasional command based on rate' ) do|s|
        options[:discard] = s
    end
  
    # This displays the help screen, all programs are
    # assumed to have this option.
    opts.on( '-h', '--help', 'Display this screen' ) do
        puts opts
        exit
    end
end

 

# script really starts here...

iniSettings = ""

iniFile = File.expand_path(File.dirname(__FILE__))+"/VMXProxy.ini"
iniFile = $1+".ini" if ENV['OCRA_EXECUTABLE']=~/^(.*)\..*?$/
iniSettings = File.read(iniFile) if File.exist?(iniFile)
iniSettings.gsub!(/\#.*$/,"")       # remove comments

# options are read from iniFile if no arguments are given
iniFile_args=$1 if iniSettings =~ /^ARGS: (.*)$/ && ARGV.length==0
optparse.default_argv = $1.split(" ") if iniFile_args
optparse.parse!
#puts options


if options[:serial] and options[:net]
    puts "VMXProxy v"+VERSION+" [Proxy mode]"
    proxy_mode = true
    sim_mode = false
elsif options[:serial] or options[:net]
    puts "VMXProxy v"+VERSION+" [Simulator mode]"
    proxy_mode=false
    sim_mode=true
else 
    abort "Need to supply either or both --net and --serial options"
end

puts "VMXProxy.ini file detected" if iniSettings.length > 0
puts "Taking configuation arguments from VMXProxy.ini file: "+iniFile_args if iniFile_args

mVMXConsumerClient = VMixerConsumer.new()
mVMXConsumerMixer = VMixerConsumer.new() if proxy_mode


mixer_scene = "023,\"Default\""
mixer_scene = $1 if iniSettings =~ /^SCENE:\s+(\d\d\d,\".*\")\s*$/
mVMXConsumerClient.mixer_scene = mixer_scene
puts "Scene is "+mixer_scene

channelNames = $1 if iniSettings =~ /^CHANNELS\s*$(.*)^ENDCHANNELS\s*$/m
if channelNames
    channels=0
    channelNames.each_line { |s|
        unless s =~ /^\s*$/
            channels += 1
            channelId = format("I%d", channels)
            mixer_name = s.gsub(/^\s*/,"").gsub(/\s*$/,"")
            mVMXConsumerClient.mixer_name[channelId] = format("%-6.6s", mixer_name)
            #puts channelId+" : "+mVMXConsumerClient.mixer_name[channelId]+" :FF"
        end
    }
    puts "Detected Names of #{channels} Channels"
else
    for i in 1..48
        channelId = format("I%d", i)
        mixer_name = format("CH%4s", channelId)
        mVMXConsumerClient.mixer_name[channelId] = format("%-6.6s", format("CH%4s", channelId))
        puts channelId+" : "+mVMXConsumerClient.mixer_name[channelId]+" :AG"
    end
end

mVMXConsumerClient.verbose = options[:verbose];
mVMXConsumerMixer.verbose = options[:verbose] if mVMXConsumerMixer;


printf "DEBUG MODE: Applying delay for each command upto " + options[:delay] + "ms\n" if options[:delay]
printf "DEBUG MODE: Discard 1 in " + options[:discard] + " commands on input\n" if options[:discard]


if options[:serial] 
    printf "Serial Port: %s %d %d %d\n", options[:serial], options[:baud_rate], options[:data_bits], options[:stop_bits]
    serialSocket = SerialPort.new( options[:serial], options[:baud_rate], options[:data_bits], options[:stop_bits], options[:parity] )
    if proxy_mode
        serialSocket.read_timeout = 1000
        serialSocket.flow_control = SerialPort::SOFT
    elsif sim_mode
        serialSocket.read_timeout = 0
        serialSocket.flow_control = SerialPort::NONE
    end
end


if options[:net]
    puts "Network Port " + options[:net]
    theServerThread = Thread.new {
        system("avahi-publish -s vmxproxy _telnet._tcp "+options[:net]+" vmxproxy=1") if OS.posix?
        system("dns-sd -R vmxproxy _telnet._tcp local "+options[:net]+" vmxproxy=1") if OS.windows?
    }
    server = TCPServer.open("", options[:net])      # "" is important to open all connections (esp in Windows)
    puts "Awaiting Connection" if options[:net]
end

theServerThread = nil
loop do

    if options[:net]
        clientSocket = server.accept
    else
        clientSocket = serialSocket
    end

    if theServerThread
        # BOMBPROOFING!  kill the thread if it is already running (only one allowed)
        theServerThread.raise Errno::EPIPE if theServerThread.alive?
    end

    theServerThread = Thread.new {
        begin  
            puts "Opened Connection"
            auth_fail_message_given = false
            commands_processed = 0 

            mVMXConsumerClient.reset()
            mVMXConsumerClient.requireAuthentication( options[:password] ) if options[:password]

            # keep going whilst vmixer is responding
            vmixer_responding = true
            while vmixer_responding do
                resp = ""
                begin
                    while (c = clientSocket.getc) do
                        #puts c.inspect
                        cmd = mVMXConsumerClient.gather(c)
                        break if cmd
                    end
                    raise Errno::EPIPE if c == nil    # eof

                    output_string = mVMXConsumerClient.string;

                    if options[:delay]
                        delay = (rand(options[:delay].to_i)+1).to_f/1000 if options[:delay]
                        sleep(delay)
                        output_string += " (delay "+(delay*1000).to_i.to_s+"ms)"
                    end

                    output_string += " (CONCAT)" if mVMXConsumerClient.isConcatenatedCommand
                    puts ">>SVR " + output_string if options[:verbose] 

                    # We have a command extension... <stx>###PWD: which is used to supply password
                    if mVMXConsumerClient.isCommandExtension
                        puts "Command Extension Detected (bypasses server/sim)"
                        resp += mVMXConsumerClient.parse
                        puts "Client is" + ( mVMXConsumerClient.isAuthenticated ? " " : " not " ) + "authenticated."
                        # it is a good idea not to pass on command extenstions to sim or server

                    elsif not mVMXConsumerClient.isAuthenticated
                        puts "Command ignored as not authenticated\n" unless auth_fail_message_given
                        auth_fail_message_given = true unless options[:verbose]
                        # commands are ignored (no reply) to encourage a disconnect

                    elsif options[:discard] && rand(options[:discard].to_i)==1
                        puts("DEBUG MODE: Discarding command")

                    elsif sim_mode
                        resp += mVMXConsumerClient.parse

                    elsif proxy_mode
                        puts "  >>VMX "+mVMXConsumerClient.string if options[:verbose]
                        serialSocket.write(cmd)
                        serialSocket.flush
                        # gather the response
                        # would use .getc, but doesn't seem to work with read_timeout value set in Windows, so using readpartial(1) instead.
                        begin
                            while (c = serialSocket.readpartial(1)) do
                                vmxResp = mVMXConsumerMixer.gather(c)
                                if vmxResp
                                    resp += vmxResp
                                    puts "  <<VMX "+mVMXConsumerMixer.string if options[:verbose]
                                    break
                                end
                            end
                        rescue EOFError
                            puts "  VMX timeout!"
                            vmixer_responding = false;
                            break;
                        end

                    end
                end while mVMXConsumerClient.isConcatenatedCommand

                if vmixer_responding and not resp.empty?
                    puts "<<SVR " + mVMXConsumerClient.string(resp) if options[:verbose]
                    clientSocket.write(resp)
                    clientSocket.flush
                    commands_processed += 1
                end
            end  # of VMixer Read, Write loop

        rescue Errno::EPIPE
            puts "Connection Closed (#{commands_processed} commands processed)"
        rescue Exception => myException
            puts "Exception rescued : #{myException}"
            puts "Connection Closed (#{commands_processed} commands processed)"
        end

        clientSocket.close
    }    # end of thread

    # if not proxy or network sim, run single threaded for serial sim
    theServerThread.join unless options[:net]

end

