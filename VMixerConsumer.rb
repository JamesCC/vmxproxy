class VMixerConsumer

    attr_reader :isAuthenticated
    attr_reader :isConcatenatedCommand
    
    attr_accessor :mixer_name
    attr_accessor :mixer_scene
    attr_writer :verbose

    def initialize
        @ack = "\x06"
        @stx = "\x02"
        
        # mixer default values
        # valid operands used to index hash - I1–I32, AX1–AX8, MX1–MX4, MAL, MAR, MAC, DCA1–DCA4, MG1–MG4, U1–U32
        # levels of -100 are treated as infinity
        @mixer_vol = Hash.new( 0.0 )
        @mixer_mute = Hash.new( false )
        @mixer_phase = Hash.new( false )
        @mixer_eq = Hash.new( false )
        @mixer_hpf = Hash.new( false )
        @mixer_gain = Hash.new( -15.0 )
        @mixer_pad = Hash.new( false )
        @mixer_phantom = Hash.new( false )
        @mixer_auxsend = Hash.new( -80.0 )

        @mixer_name = Hash.new("Spare ")
        @mixer_scene = "000,\"default\""

        @verbose = false
        reset()
    end

    def reset()
        @state = 0
        @stash = ""
        @insideQuotes = false
        @resp = ""
        @isAuthenticated = true;
        @cmdFound = ""
        @isConcatenatedCommand = false
    end

    def requireAuthentication( password )
        @password = password
        @isAuthenticated = false
    end

    def gather( c )
        c = @stx if c == "$"         # $ can be used as STX (useful for testing)
        if c == @stx or c == @ack
            @state = 1
            @stash = ""
            @insideQuotes = false
            @isConcatenatedCommand = false
        end

        if @state == 1
            @insideQuotes = !@insideQuotes if c == "\""
            @stash.concat(c)
            if not @insideQuotes
                if c == @ack or c == ";"
                     @state = 0
                     @isConcatenatedCommand = false
                     @cmdFound = @stash
                     return @cmdFound
                elsif c == "&"
                     # for concatenated commands prefix stx for next cmd
                     @cmdFound = @stash.clone
                     @cmdFound[-1] = ";"           # replace & with ;
                     @stash = @stx.clone
                     @isConcatenatedCommand = true
                     return @cmdFound
                end
            end
        else
            puts("junk: " + c.inspect) if @verbose;
        end

        return nil
    end

    def isCommandExtension
        @cmdFound =~ /^#{@stx}\#\#\#(.*)[;&]$/
    end
      
    def parse
        # default response is error
        @resp = @stx + "ERR:0;"

        if not @cmdFound=~/^#{@stx}.*[;&]$/
            return @resp
        end
        @cmdFound = @cmdFound[1..-1]          # drop leading stx

        # if error is received repeat last response
        if @cmdFound =~ /^ERR:5;$/
            return @resp
        end

        # We have unofficially extended the command syntax to facilitate
        # a new command to supply the password.  <stx>###PWD:password;
        # This is done to maintain the packetisation parsing, making 
        # the password autentication more robust against pkt splitting.
        if @cmdFound =~ /^\#\#\#(...):([^,;]+);$/
            opcode = $1
            p1 = $2
            if opcode == "PWD"          # password
                @isAuthenticated |= @password.eql?(p1)
                @resp = (@isAuthenticated ? @ack : @stx+"ERR:6;")
            end
        end

        return @resp unless @isAuthenticated 


        # The following is all related to simulating the vmixer console
        # it is not intended to be highly accurate, but intended to be 
        # enough to validate the application, and show it in action.

        # control commands (1 param)
        if @cmdFound =~ /^(...):([^,;]+),([^,;]+);$/
            opcode = $1
            operand = $2
            p1 = $3
            if opcode == "MUC"          # set mute state
                @mixer_mute[operand] = ( p1 == "1" )
                @resp = @ack
            elsif opcode == "PTC"       # set phantom state
                @mixer_phantom[operand] = ( p1 == "1" )
                @resp = @ack
            elsif opcode == "PSC"       # set phase state
                @mixer_phase[operand] = ( p1 == "1" )
                @resp = @ack
            elsif opcode == "EQC"       # set filter enable state
                @mixer_eq[operand] = ( p1 == "1" )
                @resp = @ack
            elsif opcode == "FLC"       # set high pass filter state
                @mixer_hpf[operand] = ( p1 == "1" )
                @resp = @ack
            elsif opcode == "FDC"       # abs fader set
                if p1 == "INF"
                    @mixer_vol[operand] = -100
                else
                    @mixer_vol[operand] = p1.to_i
                end
                @resp=@ack
            elsif opcode == "RFC"       # relative fader adj
                @mixer_vol[operand] += p1.to_i
                @resp = @ack
            end

        # control commands (2 param)
        elsif @cmdFound =~ /^(...):([^,;]+),([^,;]+),([^,;]+);$/
            opcode = $1
            operand = $2
            p1 = $3
            p2 = $4
            if opcode == "PGC"       # abs fader set
                @mixer_pad[operand] = ( p1 == "1" )
                @mixer_gain[operand] = p2.to_i
                # todo qualify parameters and return ERR:5 if necessary
                @resp = @ack
            end

        # control commands (3 param)
        elsif @cmdFound =~ /^(...):([^,;]+),([^,;]+),([^,;]+),([^,;]+);$/
            opcode = $1
            operand = $2
            p1 = $3
            p2 = $4
            p3 = $5
            if opcode == "AXC"       # aux send set
                # p1 is send output
                if p2 == "INF"
                    @mixer_auxsend[operand+p1] = -100
                else
                    @mixer_auxsend[operand+p1] = p2.to_i
                end
                # deliberately ignore panning
                @resp = @ack
            end

        end

        # request commands
        if @cmdFound =~ /^(...):([^,;]+);$/
            opcode = $1
            operand = $2
            if opcode == "MUQ"          # channel mute state
                @resp = sprintf(@stx+"MUS:%s,%d;", operand, @mixer_mute[operand]?1:0);
            elsif opcode == "PTQ"       # channel phantom state
                @resp = sprintf(@stx+"PTS:%s,%d;", operand, @mixer_phantom[operand]?1:0);
            elsif opcode == "PGQ"       # channel pad and gain state
                @resp = sprintf(@stx+"PGS:%s,%d,%d;", operand, @mixer_pad[operand]?1:0, @mixer_gain[operand]);
            elsif opcode == "FDQ"       # fader state
                s = sprintf("%2.1f", @mixer_vol[operand]);
                s = "INF" if @mixer_vol[operand] <= -100
                @resp = sprintf(@stx+"FDS:%s,%s;", operand, s);
            elsif opcode == "PSQ"       # channel phase state
                @resp = sprintf(@stx+"PSS:%s,%d;", operand, @mixer_phase[operand]?1:0);
            elsif opcode == "EQQ"       # channel equal enable state
                @resp = sprintf(@stx+"EQS:%s,%d;", operand, @mixer_eq[operand]?1:0);
            elsif opcode == "FLQ"       # channel equal enable state
                @resp = sprintf(@stx+"FLS:%s,%d;", operand, @mixer_hpf[operand]?1:0);
            elsif opcode == "CNQ"       # channel name
                @resp = sprintf(@stx+"CNS:%s,\"%-4s\";", operand, @mixer_name[operand])
            elsif opcode == "PIQ"       # channel name
                @resp = sprintf(@stx+"PIS:%s,RA%s;", operand, operand)
            end

        # request commands (1 param)
        elsif @cmdFound =~ /^(...):([^,;]+),([^,;]+);$/
            opcode = $1
            operand = $2
            p1 = $3
            if opcode == "AXQ"          # channel aux send request
                # we ignore aux send panning
                s = sprintf("%2.1f", @mixer_auxsend[operand+p1]);
                s = "INF" if @mixer_auxsend[operand+p1] <= -100
                @resp = sprintf(@stx+"AXS:%s,%s,%s,0;", operand, p1, s)
            end

        # request command (no operand)
        elsif @cmdFound =~ /^(...);$/
            opcode = $1
            if opcode == "VRQ"       # version request
                @resp = sprintf(@stx+"VRS:1.010,1.010,0.000;")
            elsif opcode == "SCQ"       # version request
                @resp = sprintf(@stx+"SCS:"+@mixer_scene+";");
            end
        end
       
        return @resp 
    end

    def string( ss = nil )
        ss = @cmdFound.clone if ss == nil
        s = ss.clone
        s.gsub!(@stx,"<stx>")
        s.gsub!(@ack,"<ack>")
        return s
    end

end

